<button onclick="document.getElementById('login').style.display='block'" style="width:auto;">Login</button>

<div id="login" class="modal">
  
  <form class="modal-content animate" action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="POST">
    <div class="imgcontainer">
      <span onclick="document.getElementById('login').style.display='none'" class="close" title="Close Modal">&times;</span>
      <img src="clg.png" alt="Avatar" class="avatar">
    </div>

    <div class="container">
      <label for="username"><b>Username</b></label>
      <input type="text" placeholder="Username" name="username">

      <label for="password"><b>Password</b></label>
      <input type="password" placeholder="Password" name="password">
        
      <button type="submit" name="submit">Login</button>
      <label>
        <input type="checkbox" checked="checked" name="remember"> Se souvenir de moi
      </label>
    </div>
  </form>
</div>



<?php


$handler=mysqli_connect("localhost", "boris", "password", "NeoNess");

$errors1 = array();
function protect2($handler, $param) {
    return mysqli_real_escape_string($handler, stripslashes($param));
}
if (isset($_POST['submit'])) {
    $username = protect2($handler, $_POST['username']);
    $password = protect2($handler, $_POST['password']);
    $password_hash = password_hash($password, PASSWORD_BCRYPT);
    if (empty($username)) { 
        array_push($errors1, "Veuillez entrer votre username"); 
    }
    if (empty($password)) { 
        array_push($errors1, "Veuillez entrer votre password"); 
    }
    if (!empty($username) && !empty($password)) {
        $requete_select_password = "SELECT password FROM users WHERE username='$username'";
        $requete_password=mysqli_query($handler, $requete_select_password);
        $value_password=$requete_password->fetch_assoc();
        $password_bdd= $value_password['password'];
        $POSTpassword=protect2($handler, $_POST['password']);
        if (password_verify($POSTpassword, $password_bdd)) {
            if (isset($_POST['remember'])) {
                setcookie("connecté", TRUE, strtotime("1 year"));
                $_COOKIE['connecté']=TRUE;
                $select_id_user="SELECT id FROM users WHERE username='$username'";
                $requete_select_id_user=mysqli_query($handler, $select_id_user);
                $value_id_user=$requete_select_id_user->fetch_assoc();
                $id_user = $value_id_user['id'];
                setcookie('user_id', $id_user, strtotime("1 year"));
            }
            $_SESSION['connecté']=TRUE;
            if ($_SESSION['connecté']==TRUE || $_COOKIE['connecté']==TRUE) {
              header("location: accueil.php");
            }
        } else {
            array_push($errors1, "La combinaison username/password n'est pas accepté");
        } 
    }
    

    if (count($errors1) > 0) { ?>
        <div class="error">
        <?php foreach ($errors1 as $key=>$value) { ?>
            <p><?=$value; ?></p>
        <?php } ?>
        </div>
        <?php  }
} 
