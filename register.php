<button onclick="document.getElementById('inscription').style.display='block'" style="width:auto;">Register</button>

<?php


$handler=mysqli_connect("localhost", "boris", "password", "NeoNess");


$username ="";
$errors2 = array();
$errors3 = array();
function protect1($handler, $param) {
    return mysqli_real_escape_string($handler, stripslashes($param));
}
if (isset($_POST["inscription"])) {
    $username = protect1($handler, $_POST['username']);
    $password_1 = protect1($handler, $_POST['password_1']);
    $password_2 = protect1($handler, $_POST['password_2']);
    if (empty($username)) { 
        array_push($errors2, "Un username est obligatoire"); }
    if (empty($password_1)) { 
        array_push($errors2, "Un password est obligatoire"); }
    if ($password_1 !== $password_2) {
        array_push($errors2, "Les deux password ne correspondent pas");
    }
    $requete_verif = "SELECT * FROM users WHERE username='$username' LIMIT 1";
    $resultat_requete_verif = mysqli_query($handler, $requete_verif);
    $user = $resultat_requete_verif->fetch_all(MYSQLI_ASSOC);
    if ($user) {
        for ($i=0; $i<count($user); $i++) {
            if ($user[$i]['username'] === $username) {
          array_push($errors2, "L'username est déjà pris");
            }

        }
        
    }
    if (count($errors2) == 0) {
        $password_hash = password_hash($password_1, PASSWORD_BCRYPT);
        $inscrpition =strtotime(date("Y-m-d"));
        $requete_insert1 = "INSERT INTO users (username, password, inscription) VALUES 
        ('$username', '$password_hash', '$inscrpition')";
        mysqli_query($handler, $requete_insert1);
        $_SESSION['username'] = $username;
        $tmpusername=$_SESSION['username'];
        $_SESSION['loggedin'] = "Loggedin";
        $_COOKIE[$_SESSION['username']]="Loggedin";
        
    }
}
if (empty($_SESSION['loggedin']) && empty($_COOKIE['loggedin'])) {
?>
    <div id="inscription" class="modal">  
        <form class="modal-content animate" action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="POST">
            <div class="imgcontainer">
                <span onclick="document.getElementById('inscription').style.display='none'" class="close" title="Close Modal">&times;</span>
                <img src="clg.png" alt="Avatar" class="avatar">
            </div>

            <div class="container">
                <label for="username"><b>Username</b></label>
                <input type="text" placeholder="Tapez votre username" name="username">

                <label for="password_1"><b>Password</b></label>
                <input type="password" placeholder="Tapez votre password" name="password_1">
                <label for="password_2"><b>Password</b></label>
                <input type="password" placeholder="Retapez votre password" name="password_2">
                <?php  if (count($errors2) > 0) { ?>
                <div class="error">
                <?php foreach ($errors2 as $key=>$value) { ?>
                    <p><?=$value; ?></p>
                <?php } ?>
                </div>
                <?php  } ?>
                <button name="inscription">S'inscrire</button>
            </div>
        </form>
        
    </div><?php
} else {
    ?>
    <div id="inscription" class="modal">  
        <form class="modal-content animate" action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="POST">
            <div class="imgcontainer">
                <span onclick="document.getElementById('inscription').style.display='none'" class="close" title="Close Modal">&times;</span>
                <img src="clg.png" alt="Avatar" class="avatar">
                <p>Il nous manque encore quelques informations pour pouvoir utiliser l'appli</p>
            </div>

            <div class="container">
                <label for="prenom">Prénom</label><br>
                <input type="text" name="prenom" placeholder="Prénom">

                <label for="nom">Nom</label>
                <input type="text" name="nom" placeholder="Nom">

                <label for="num_tel">Numéro de Téléphone</label>
                <input type="tel" name="num_tel" placeholder="Téléphone">

                <label for="anniversaire">Date d'anniversaire</label>
                <input type="date" name="anniversaire">

                <label for="poids">Votre poids en kg *</label>
                <input type="number" name="poids">

                <label for="taille">Votre taille en cm *</label>
                <input type="number" name="taille">
                <input type="hidden" id="username" name="username" value="<?=$username?>">

                <?php  if (count($errors2) > 0) { ?>
                <div class="error">
                <?php foreach ($errors2 as $key=>$value) { ?>
                    <p><?=$value; ?></p>
                <?php } ?>
                </div>
                <?php  } ?>
                <button type="submit" name="infos">Valider</button>
                <p>* Champs obligatoire</p>
                <?php  if (count($errors3) > 0) { ?>
                <div class="error">
                <?php foreach ($errors3 as $key2=>$value2) { ?>
                <p><?= $value2; ?></p>
                <?php } ?>
                </div>
                <?php  } ?>
            </div>
        </form>
    </div>
    <?php
}
?>
    <?php


if (isset($_POST["infos"])) {
        $user_prenom = protect1($handler,$_POST['prenom']);
        $user_nom = protect1($handler, $_POST['nom']);
        $user_num_tel = protect1($handler, $_POST['num_tel']);
        $user_anniversaire = protect1($handler, strtotime($_POST['anniversaire']));
        $user_poids = protect1($handler, $_POST['poids']);
        $user_taille = protect1($handler, $_POST['taille']);
        $aujourdhui=  protect1($handler, strtotime(date('Y-m-d')));
        $username = protect1($handler, $_POST['username']);
        if (empty($user_poids)) { 
            array_push($errors3, "Veuillez entrez des informations dans les champs obligatoires"); 
        }
        
        if ($user_anniversaire >= $aujourdhui) {
            array_push($errors3, "Veuillez entrez une date inferieure à celle d'aujourd'hui"); 
        }
        
        if (count($errors3) == 0) {
        
            $requete_update_user = "UPDATE users 
                            SET prenom='$user_prenom', nom='$user_nom', 
                            num_tel='$user_num_tel', 
                            anniversaire='$user_anniversaire', taille='$user_taille' 
                            WHERE username='$username'";
            mysqli_query($handler, $requete_update_user);
            
            $select_id_user="SELECT id FROM users WHERE username='$username'";
            $requete_select_id_user=mysqli_query($handler, $select_id_user);
            $value_id_user=$requete_select_id_user->fetch_assoc();
            $id_user = $value_id_user['id'];
            
            $select_id_poids="SELECT id FROM liste_poids WHERE poids='$user_poids'";
            $requete_select_id_poids=mysqli_query($handler, $select_id_poids);
            $value_id_poids=$requete_select_id_poids->fetch_assoc();
            $id_poids= $value_id_poids['id'];
            

            $requete_insert_poids="INSERT INTO historique_poids (id_user, id_poids, date)
            VALUE ('$id_user', '$id_poids', '$aujourdhui')";
            mysqli_query($handler, $requete_insert_poids);
            $_SESSION['connecté']=TRUE;
            $_COOKIE['connecté']=TRUE;
            $_COOKIE['user_id']=$id_user;
            echo "Vous pouvez vous connecter à présent";
            }
        }


        