<head>
  <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>
  <link rel="stylesheet" type="text/css" href="styles.css" media="all"/>
</head>
<style>
  .bouton_header {
    background-color: darkslateblue;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 100px;
  }
</style>
<?php
session_start();
$handler=mysqli_connect("localhost", "boris", "password", "NeoNess");

if (($_SESSION['connecté']==FALSE) && ($_COOKIE['connecté']==FALSE)) {
  header('location:index.php');
}

if (isset($_POST['disconnect'])) {
  $_SESSION['connecté']=FALSE;
  setcookie("connecté", FALSE);
  $_COOKIE['connecté']=FALSE;
  $_SESSION['loggedin']="";
  $_COOKIE['loggedin']="";
  header('location:index.php');


}


$id_user=$_COOKIE['user_id'];

$errors = array();
function protect($handler, $param) {
  return mysqli_real_escape_string($handler, stripslashes($param));
}
function getname($handler, $id){
  $select_prenom="SELECT prenom FROM users WHERE id=$id";
  $requete_select_prenom=mysqli_query($handler, $select_prenom);
  $value_prenom=$requete_select_prenom->fetch_assoc();
  $prenom= $value_prenom['prenom'];
  return $prenom;
}


?>
<header>
  <div>
    <form action="<?=$_SERVER["PHP_SELF"]; ?>"method="POST">
    <button class='bouton_header' type="submit" name="disconnect">Disconnect</button>
    </form>
  </div>
  <?php
  if ($id_user==48) {
    ?>
      <div>
      <form action="<?=$_SERVER["PHP_SELF"]; ?>"method="POST">
        <button class='bouton_header' type="submit" name="admin">Super Admin</button>
      </form>
    
    </div>
  <?php
  if (isset($_POST['admin'])) {
    header('location:admin.php');
  }
}
?>
</header>
<main>
  <h1>Bonjour <?= getname($handler, $_COOKIE['user_id']) ?></h1>
  <h1>Bienvenue sur l'appli NeoNess</h1>
  <?php 
  $select_user_poids="SELECT poids
                    FROM historique_poids
                    INNER JOIN liste_poids ON historique_poids.id_poids=liste_poids.id
                    WHERE historique_poids.id_user='$id_user'
                    ORDER BY date DESC LIMIT 1";
  $requete_select_user_poids=mysqli_query($handler, $select_user_poids);
  $value_user_poids=$requete_select_user_poids->fetch_assoc();
  $user_poids = $value_user_poids['poids'];
  $select_user_taille="SELECT taille FROM users WHERE id='$id_user'";
  $requete_select_user_taille=mysqli_query($handler, $select_user_taille);
  $value_user_taille=$requete_select_user_taille->fetch_assoc();
  $user_taille = $value_user_taille['taille'];
  
  $user_taille_metre=$user_taille/100;
  $imc=$user_poids/$user_taille_metre**2;
  ?>
  <h3>Votre IMC actuel est de: <?= number_format($imc,1) ?></h3>

  <button onclick="document.getElementById('actualiser_poids').style.display='block'" style="width:auto;">Actualiser votre poids</button>

  <div id="actualiser_poids" class="modal">
  
    <form class="modal-content animate" action="<?= $_SERVER["PHP_SELF"]; ?>" method="POST">
      <div class="imgcontainer">
        <span onclick="document.getElementById('actualiser_poids').style.display='none'" class="close" title="Close Modal">&times;</span>
        <img src="clg.png" alt="Avatar" class="avatar">
      </div>

      <div class="container">
     
        <label for="poids"><b>Votre poids</b></label>
        <input type="number" name="poids">

        <label for="date_poids">Date</label>
        <input type="date" name="date_poids">
        
        <button type="submit" name="submit_poids">Valider</button>
      </div>
    </form>
  </div>
  <?php 
  if (isset($_POST['submit_poids'])) {
    $aujourdhui=  protect($handler, strtotime(date('Y-m-d')));
    $id_user=$_COOKIE['user_id'];
    $nouveau_poids=protect($handler, $_POST['poids']);
    $date_poids=protect($handler, strtotime($_POST['date_poids']));
    if ($date_poids<=$aujourdhui) {
      $select_id_poids="SELECT id FROM liste_poids WHERE poids='$nouveau_poids'";
      $requete_select_id_poids=mysqli_query($handler, $select_id_poids);
      $value_id_poids=$requete_select_id_poids->fetch_assoc();
      $id_poids= $value_id_poids['id'];
      $requete_insert_poids="INSERT INTO historique_poids (id_user, id_poids, date)
                VALUE ('$id_user', '$id_poids', '$date_poids')";
      $insert_poids=mysqli_query($handler, $requete_insert_poids);
      if ($insert_poids==false) {
        $requete_update_poids="UPDATE historique_poids 
                              SET date='$date_poids' 
                              WHERE id_user='$id_user' && id_poids='$id_poids'";
        mysqli_query($handler, $requete_update_poids);
      }
    } else {
      array_push($errors, "Veuillez introduire votre poids à une date égale ou inferieur à aujourd'hui");
    }




  }
?>
  <button onclick="document.getElementById('objectif_poids').style.display='block'" style="width:auto;">Se créer un objectif</button>

  <div id="objectif_poids" class="modal">

    <form class="modal-content animate" action="<?= $_SERVER["PHP_SELF"]; ?>" method="POST">
      <div class="imgcontainer">
        <span onclick="document.getElementById('objectif_poids').style.display='none'" class="close" title="Close Modal">&times;</span>
        <img src="clg.png" alt="Avatar" class="avatar">
      </div>

      <div class="container">
    
        <label for="obj_poids"><b>Votre objectif poids</b></label>
        <input type="number" name="obj_poids">

        <label for="date_poids">Date de l'objectif</label>
        <input type="date" name="date_poids">
        
        <button type="submit" name="submit_objectif_poids">Valider</button>
      </div>
    </form>
  </div>
  <?php 
  if (isset($_POST["submit_objectif_poids"])) {
    $id_user=$_COOKIE['user_id'];
    $objectif_poids=protect($handler, $_POST['obj_poids']);
    $date_obj_poids=protect($handler, strtotime($_POST['date_poids']));
    $aujourdhui= protect($handler, strtotime(date('Y-m-d')));
    if ($date_obj_poids>$aujourdhui) {
      $select_id_poids="SELECT id FROM liste_poids WHERE poids='$objectif_poids'";
      $requete_select_id_poids=mysqli_query($handler, $select_id_poids);
      $value_id_poids=$requete_select_id_poids->fetch_assoc();
      $id_poids= $value_id_poids['id'];
      $insert_objectif_poids="INSERT INTO user_obj_poids (id, obj_poids, date_obj) 
                              VALUE ('$id_user', '$id_poids', '$date_obj_poids')";
      mysqli_query($handler, $insert_objectif_poids);
    } else {
      array_push($errors, "Vous ne pouvez pas fixer d'objectif de poids à une date égale ou inferieur à aujourd'hui");
    }
  }


  ?>

  <button onclick="document.getElementById('exercice').style.display='block'" style="width:auto;">Ajouter un exercice</button>

  <div id="exercice" class="modal">
    
    <form class="modal-content animate" action="<?=$_SERVER["PHP_SELF"]; ?>" method="POST">
      <div class="imgcontainer">
        <span onclick="document.getElementById('exercice').style.display='none'" class="close" title="Close Modal">&times;</span>
        <img src="clg.png" alt="Avatar" class="avatar">
      </div>

      <div class="container">
        <label for="sport"><b>Sport</b></label>
        <select name="sport" size="1">
          <option>Aviron
          <option>Badminton
          <option>Football
          <option>Course de fond
          <option>Haltère
          <option>Natation
          <option>Saute-Mouton
          <option>Sprint
          <option>Tennis
        </select>

        <label for="duree"><b>Temps en Minutes</b></label>
        <input type="number" name="duree">
          
        <button type="submit" name="submitExercice">Valider</button>
      </div>
    </form>
  </div>
  <?php 
  if (isset($_POST['submitExercice'])) {
    $id_user=$_COOKIE['user_id'];
    $nom_sport=protect($handler, $_POST['sport']);
    $select_id_sport="SELECT id FROM liste_sport WHERE nom='$nom_sport'";
    $requete_select_id_sport=mysqli_query($handler, $select_id_sport);
    $value_id_sport=$requete_select_id_sport->fetch_assoc();
    $id_sport = $value_id_sport['id'];
    $duree=protect($handler, $_POST['duree']);
    if ($duree>0) {
      $requete_insert_exercice="INSERT INTO exercice (id_users, id_sport, duree) 
                                VALUE ('$id_user', '$id_sport', '$duree')";
      mysqli_query($handler, $requete_insert_exercice);
    } else {
      array_push($errors, "Vous ne pouvez pas ajouter d'exercice d'une durée inferieur à 0");
    }

  }
?>
<button onclick="document.getElementById('tableau_historique_poids1').style.display='block'" style="width:auto;">Historique de poids</button>

<?php  if (count($errors) > 0) { ?>
                <div class="error">
                <?php foreach ($errors as $key2=>$value) { ?>
                <p><?= $value; ?></p>
                <?php } ?>
                </div>
                <?php  }
                
                


  $select_historique_poids="SELECT poids FROM historique_poids 
                inner join liste_poids on historique_poids.id_poids=liste_poids.id
                where id_user='$id_user'
                order by date asc";
  $requete_select_historique_poids=mysqli_query($handler, $select_historique_poids);
  $value_historique_poids=$requete_select_historique_poids->fetch_all();
  $tableau_user_poids = array();
  for ($i=0; $i<count($value_historique_poids); $i++) {
    $tmp1=$value_historique_poids[$i][0];
    array_push($tableau_user_poids, $tmp1);
  }
  $chaine_liste_poids=implode(", ", $tableau_user_poids);
  $select_historique_date="SELECT date FROM historique_poids 
                          inner join liste_poids on historique_poids.id_poids=liste_poids.id
                          where id_user='$id_user'
                          order by date asc";
  $requete_select_historique_date=mysqli_query($handler, $select_historique_date);
  $value_historique_date=$requete_select_historique_date->fetch_all();
  $tableau_user_date=array();
  for ($i=0; $i<count($value_historique_date); $i++) {
    $tmp2=date("'d m Y'", $value_historique_date[$i][0]);
    array_push($tableau_user_date, $tmp2);
  }
  $chaine_liste_date=implode(", ", $tableau_user_date);

  $select_objectif_poids= "SELECT poids FROM user_obj_poids
                          inner join liste_poids on user_obj_poids.obj_poids=liste_poids.id
                          where user_obj_poids.id='$id_user'";
  $requete_select_objectif_poids=mysqli_query($handler, $select_objectif_poids);
  $value_objectif_poids=$requete_select_objectif_poids->fetch_assoc();
  $objectif_poids = $value_objectif_poids['poids'];
  $tableau_objectif_poids=array();
  for ($i=0; $i<count($tableau_user_poids); $i++) {
    array_push($tableau_objectif_poids, $objectif_poids);
  }
  $chaine_objectif_poids=implode(", ", $tableau_objectif_poids);

  ?>
  
  
</main>
<div id="tableau_historique_poids1" class="modal">
  <div class="container">
    <div class="imgcontainer">
      <span onclick="document.getElementById('exercice').style.display='none'" class="close" title="Close Modal">&times;</span>
    </div>
    <form class="modal-content animate">
      <canvas id="tableau_historique_poids" width="" height=""></canvas>
    </form>
  </div>
</div>

<script>
  // GET the modal
  var modal1 = document.getElementById('exercice');
  var modal2 = document.getElementById('actualiser_poids');
  var modal3 = document.getElementById('objectif_poids');
  var modal4 = document.getElementById('tableau_historique_poids1');
  // When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event) {
      if (event.target == modal1) {
          modal1.style.display = "none";
      }
      if (event.target == modal2) {
          modal2.style.display = "none";
      }
      if (event.target == modal3) {
          modal3.style.display = "none";
      }
      if (event.target == modal4) {
          modal4.style.display = "none";
      }

  }



  var ctx = document.getElementById('tableau_historique_poids').getContext('2d');
  var tableau_historique_poids = new Chart(ctx, {
    type: 'line',
    data: {
        labels: [<?= $chaine_liste_date ?>],
        datasets: [{
            label: 'Votre poids',
            data: [<?= $chaine_liste_poids ?>],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        },
        {
            label: 'Objectif',
            data: [<?= $chaine_objectif_poids?> ],
            backgroundColor: [
                'rgba(44, 130, 201, 0.2)',
                'rgba(44, 130, 201, 1)',
                'rgba(44, 130, 201, 1)',
                'rgba(44, 130, 201, 1)',
                'rgba(44, 130, 201, 1)',
                
            ],
            borderColor: [
                'rgba(44, 130, 201, 1)',
                'rgba(44, 130, 201, 1)',
                'rgba(44, 130, 201, 1)',
                'rgba(44, 130, 201, 1)',
                'rgba(44, 130, 201, 1)',
            ],
            borderWidth: 1
        }],
        
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

</script>