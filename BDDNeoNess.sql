-- MySQL dump 10.13  Distrib 5.7.30, for Linux (x86_64)
--
-- Host: localhost    Database: NeoNess
-- ------------------------------------------------------
-- Server version	5.7.30-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `exercice`
--

DROP TABLE IF EXISTS `exercice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exercice` (
  `id_users` int(11) NOT NULL,
  `id_sport` int(11) NOT NULL,
  `duree` varchar(15) NOT NULL,
  PRIMARY KEY (`id_users`,`id_sport`),
  KEY `fk_sport_idx` (`id_sport`),
  CONSTRAINT `fk_sport` FOREIGN KEY (`id_sport`) REFERENCES `liste_sport` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exercice`
--

LOCK TABLES `exercice` WRITE;
/*!40000 ALTER TABLE `exercice` DISABLE KEYS */;
INSERT INTO `exercice` VALUES (48,2,'60');
/*!40000 ALTER TABLE `exercice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `historique_poids`
--

DROP TABLE IF EXISTS `historique_poids`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `historique_poids` (
  `id_user` int(11) NOT NULL,
  `id_poids` int(11) NOT NULL,
  `date` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id_user`,`id_poids`),
  KEY `fk_id_poids_idx` (`id_poids`),
  KEY `fk_id_user_idx` (`id_user`),
  CONSTRAINT `fk_id_poids` FOREIGN KEY (`id_poids`) REFERENCES `liste_poids` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_user` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `historique_poids`
--

LOCK TABLES `historique_poids` WRITE;
/*!40000 ALTER TABLE `historique_poids` DISABLE KEYS */;
INSERT INTO `historique_poids` VALUES (48,51,'1594764000'),(48,66,'1576364400'),(48,71,'1594677600'),(48,73,'1594504800'),(48,76,'1594677600'),(50,56,'1594764000');
/*!40000 ALTER TABLE `historique_poids` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `liste_poids`
--

DROP TABLE IF EXISTS `liste_poids`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `liste_poids` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poids` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `poids_UNIQUE` (`poids`)
) ENGINE=InnoDB AUTO_INCREMENT=261 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `liste_poids`
--

LOCK TABLES `liste_poids` WRITE;
/*!40000 ALTER TABLE `liste_poids` DISABLE KEYS */;
INSERT INTO `liste_poids` VALUES (12,1),(13,2),(14,3),(15,4),(16,5),(17,6),(18,7),(19,8),(20,9),(21,10),(22,11),(23,12),(24,13),(25,14),(26,15),(27,16),(28,17),(29,18),(30,19),(31,20),(32,21),(33,22),(34,23),(35,24),(36,25),(37,26),(38,27),(39,28),(40,29),(41,30),(42,31),(43,32),(44,33),(45,34),(46,35),(47,36),(48,37),(49,38),(50,39),(51,40),(52,41),(53,42),(54,43),(55,44),(56,45),(57,46),(58,47),(59,48),(60,49),(61,50),(62,51),(63,52),(64,53),(65,54),(66,55),(67,56),(68,57),(69,58),(70,59),(71,60),(72,61),(73,62),(74,63),(75,64),(76,65),(77,66),(78,67),(79,68),(80,69),(81,70),(82,71),(83,72),(84,73),(85,74),(86,75),(87,76),(88,77),(89,78),(90,79),(91,80),(92,81),(93,82),(94,83),(95,84),(96,85),(97,86),(98,87),(99,88),(100,89),(101,90),(102,91),(103,92),(104,93),(105,94),(106,95),(107,96),(108,97),(109,98),(110,99),(111,100),(112,101),(113,102),(114,103),(115,104),(116,105),(117,106),(118,107),(119,108),(120,109),(121,110),(122,111),(123,112),(124,113),(125,114),(126,115),(127,116),(128,117),(129,118),(130,119),(131,120),(132,121),(133,122),(134,123),(135,124),(136,125),(137,126),(138,127),(139,128),(140,129),(141,130),(142,131),(143,132),(144,133),(145,134),(146,135),(147,136),(148,137),(149,138),(150,139),(151,140),(152,141),(153,142),(154,143),(155,144),(156,145),(157,146),(158,147),(159,148),(160,149),(161,150),(162,151),(163,152),(164,153),(165,154),(166,155),(167,156),(168,157),(169,158),(170,159),(171,160),(172,161),(173,162),(174,163),(175,164),(176,165),(177,166),(178,167),(179,168),(180,169),(181,170),(182,171),(183,172),(184,173),(185,174),(186,175),(187,176),(188,177),(189,178),(190,179),(191,180),(192,181),(193,182),(194,183),(195,184),(196,185),(197,186),(198,187),(199,188),(200,189),(201,190),(202,191),(203,192),(204,193),(205,194),(206,195),(207,196),(208,197),(209,198),(210,199),(211,200),(212,201),(213,202),(214,203),(215,204),(216,205),(217,206),(218,207),(219,208),(220,209),(221,210),(222,211),(223,212),(224,213),(225,214),(226,215),(227,216),(228,217),(229,218),(230,219),(231,220),(232,221),(233,222),(234,223),(235,224),(236,225),(237,226),(238,227),(239,228),(240,229),(241,230),(242,231),(243,232),(244,233),(245,234),(246,235),(247,236),(248,237),(249,238),(250,239),(251,240),(252,241),(253,242),(254,243),(255,244),(256,245),(257,246),(258,247),(259,248),(260,249);
/*!40000 ALTER TABLE `liste_poids` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `liste_sport`
--

DROP TABLE IF EXISTS `liste_sport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `liste_sport` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `liste_sport`
--

LOCK TABLES `liste_sport` WRITE;
/*!40000 ALTER TABLE `liste_sport` DISABLE KEYS */;
INSERT INTO `liste_sport` VALUES (1,'Aviron'),(2,'Badminton'),(3,'Course de Fond'),(4,'Football'),(5,'Gym'),(6,'Haltère'),(7,'Natation'),(8,'Saute-Mouton'),(9,'Sprint'),(10,'Tennis');
/*!40000 ALTER TABLE `liste_sport` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_obj_poids`
--

DROP TABLE IF EXISTS `user_obj_poids`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_obj_poids` (
  `id` int(11) NOT NULL DEFAULT '0',
  `obj_poids` int(11) NOT NULL,
  `date_obj` varchar(15) NOT NULL,
  PRIMARY KEY (`id`,`obj_poids`),
  KEY `fk_poids_id_idx` (`obj_poids`),
  CONSTRAINT `fk_poids_id` FOREIGN KEY (`obj_poids`) REFERENCES `liste_poids` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_id` FOREIGN KEY (`id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_obj_poids`
--

LOCK TABLES `user_obj_poids` WRITE;
/*!40000 ALTER TABLE `user_obj_poids` DISABLE KEYS */;
INSERT INTO `user_obj_poids` VALUES (48,71,'1594764000'),(48,81,'1594764000');
/*!40000 ALTER TABLE `user_obj_poids` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(60) NOT NULL,
  `prenom` varchar(45) DEFAULT NULL,
  `nom` varchar(45) DEFAULT NULL,
  `num_tel` varchar(15) DEFAULT NULL,
  `anniversaire` varchar(15) DEFAULT NULL,
  `taille` int(11) DEFAULT NULL,
  `obj_poids` int(11) DEFAULT NULL,
  `inscription` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `num_tel_UNIQUE` (`num_tel`),
  KEY `fk_user_obj_poids_idx` (`obj_poids`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (48,'VieuxPoulpe','$2y$10$2zNGC5PSVhXN5XxNYC3E8.Uv0dJ8XuHNuFSJL9oycv/Zk7N4OYjMu','Boris','AHUMADA','0636943011','786150000',172,NULL,1594591200),(50,'beweb','$2y$10$5avZIBIAhhYjvv4aN277P.bPgr8B1dt4gfto9Fmjo0K16EeEOOvOW','bonjour','hhj','4545','647992800',150,NULL,1594764000);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-15 12:19:55
