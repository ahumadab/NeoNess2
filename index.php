<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="styles.css" media="all"/>
  </head>
    <?php
    session_start();
    include "login.php";
    include "register.php";
    ?>
  <script>
    // Get the modal
    var modal1 = document.getElementById('inscription');
    var modal2 = document.getElementById('login');
    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal1 || event.target == modal2) {
            modal1.style.display = "none";
            modal2.style.display = "none";
        }
    }
  </script>
